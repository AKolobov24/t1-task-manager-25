package ru.t1.akolobov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String middleName;

    @NotNull
    private String email;

    @NotNull
    private Role role = Role.USUAL;

    private boolean locked = false;

}
