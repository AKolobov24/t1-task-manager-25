package ru.t1.akolobov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.model.IHasCreated;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractModel implements IHasCreated {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

}
