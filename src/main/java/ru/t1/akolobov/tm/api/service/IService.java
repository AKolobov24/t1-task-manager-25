package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    void clear();

    boolean existById(@Nullable String id);

    @NotNull
    List<M> findAll(@Nullable Comparator<? super M> comparator);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M remove(@Nullable M model);

    @NotNull
    M removeById(@Nullable String id);

    @NotNull
    M removeByIndex(@Nullable Integer index);

}
