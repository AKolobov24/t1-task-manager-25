package ru.t1.akolobov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    void clear();

    boolean existById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<? super M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    Integer getSize();

    @NotNull
    M remove(@NotNull M model);

    @NotNull
    M removeById(@NotNull String id);

    @NotNull
    M removeByIndex(@NotNull Integer index);

}
