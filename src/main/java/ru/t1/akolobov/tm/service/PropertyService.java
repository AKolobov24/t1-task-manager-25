package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "author.name";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "1324";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "4123";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return getIntValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return getStringValue(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getApplicationAuthor() {
        return getStringValue(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return getStringValue(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        // в параметрах запуска
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        // в переменных среды
        @NotNull String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        // в файле настройки
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private Integer getIntValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

}
