package ru.t1.akolobov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.akolobov.tm.api.repository.ICommandRepository;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.*;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.command.system.ApplicationExitCommand;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.akolobov.tm.exception.system.CommandNotSupportedException;
import ru.t1.akolobov.tm.repository.CommandRepository;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.TaskRepository;
import ru.t1.akolobov.tm.repository.UserRepository;
import ru.t1.akolobov.tm.service.*;

import java.lang.reflect.Modifier;
import java.util.Scanner;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru/t1/akolobov/tm/command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            userRepository,
            projectRepository,
            taskRepository,
            propertyService
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initDemoData() {
        projectService.create("1", "TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("1", "DEMO PROJECT", Status.COMPLETED);
        projectService.create("2", "BETA PROJECT", Status.NOT_STARTED);
        projectService.create("2", "BEST PROJECT", Status.IN_PROGRESS);
        taskService.create("1", "TEST TASK", Status.IN_PROGRESS);
        taskService.create("1", "DEMO TASK", Status.COMPLETED);
        taskService.create("2", "BETA TASK", Status.NOT_STARTED);
        taskService.create("2", "BEST TASK", Status.IN_PROGRESS);
        userService.create("user1", "user1", "user@mail.ru").setId("2");
        userService.create("akolobov", "akolobov", Role.ADMIN).setId("1");
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(
                () -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        @NotNull final Scanner scanner = new Scanner(System.in);
        @Nullable String command;
        @Nullable AbstractCommand abstractCommand = null;
        while (!(abstractCommand instanceof ApplicationExitCommand)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                abstractCommand = processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private AbstractCommand processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
        return abstractCommand;
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void exit() {
        System.exit(0);
    }

}
